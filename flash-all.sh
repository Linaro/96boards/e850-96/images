#!/bin/bash

set -e

images_dir=images
usb_role=host
dtbo_only=false

print_usage() {
	echo "Usage: $0 [--dtbo-only] [host | peri]"
	echo
	echo "Script for flashing all Android images to E850-96 board."
	echo
	echo "Options:"
	echo "  --dtbo-only: Flash only DTBO image; useful to switch USB role"
	echo "  host: Enable host USB; ADB will be disabled (default)"
	echo "  peri: Enable gadget USB; host USB will be disabled"
}

parse_params() {
	if [ "$1" = "--dtbo-only" ]; then
		dtbo_only=true
		shift
	fi

	if [ $# -eq 0 ]; then
		return
	fi

	if [ $# -ne 1 ]; then
		echo "Error: Invalid argument count" >&2
		print_usage $*
		exit 1
	fi

	case "$1" in
	"host" | "peri")
		usb_role=$1
		;;
	"--help")
		print_usage $*
		exit 0
		;;
	*)
		echo "Error: Invalid param '$1'" >&2
		print_usage $*
		exit 1
		;;
	esac
}

export_path() {
	export PATH=$(pwd)/host/bin:$PATH
}

do_flash_dtbo() {
	echo "---> Flashing DTBO image for \"$usb_role\" USB role..."

	fastboot flash dtbo		$images_dir/dtbo-${usb_role}.img
	fastboot reboot
}

do_flash_all() {
	echo "---> Flashing all images for \"$usb_role\" USB role..."

	fastboot flash gpt		$images_dir/gpt.img
	fastboot flash fwbl1		$images_dir/fwbl1.img
	fastboot flash epbl		$images_dir/epbl.img
	fastboot flash bl2		$images_dir/bl2.img
	fastboot flash bootloader	$images_dir/bootloader.img
	fastboot flash el3_mon		$images_dir/el3_mon.img

	fastboot flash keystorage	$images_dir/keystorage.img
	fastboot flash ldfw		$images_dir/ldfw.img
	fastboot flash tzsw		$images_dir/tzsw.img

	# Flash vbmeta.img only if AVB 2.0 is enabled
	if [ -f vbmeta.img ]; then
		fastboot flash vbmeta	$images_dir/vbmeta.img
	else
		fastboot erase vbmeta
	fi

	fastboot flash boot		$images_dir/boot.img
	fastboot flash recovery		$images_dir/recovery.img
	fastboot flash dtbo		$images_dir/dtbo-${usb_role}.img
	fastboot flash dtb		$images_dir/dtb.img

	fastboot flash super		$images_dir/super.img -S 512M

	fastboot format efs
	fastboot format metadata
	fastboot format persist
	fastboot erase modem
	fastboot flash modem		$images_dir/modem.bin
	fastboot flash logo		$images_dir/logo.bin

	fastboot -w

	fastboot reboot
}

# ---- Entry point ----

export_path
parse_params $*
if [ $dtbo_only = "true" ]; then
	do_flash_dtbo
else
	do_flash_all
fi
echo "---> Done!"
